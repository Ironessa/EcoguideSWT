const express = require('express');
const session = require('express-session');
const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const QRCode = require('qrcode');
const crypto = require('crypto');

const app = express();
const PORT = process.env.PORT || 3000;

// Serve static files from the directory one level above
app.use(express.static(path.join(__dirname, '..')));

// Route to serve frontend files
app.use('/html', express.static(path.join(__dirname, 'html')));

const db = new sqlite3.Database(path.join(__dirname, '../database/ecoguide.db'));

app.use(express.json()); // Add this line to parse JSON request bodies

// Generate a secure session secret
const sessionSecret = crypto.randomBytes(64).toString('hex');

// Session middleware configuration
app.use(session({
  secret: sessionSecret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false } // Set to true if using HTTPS
}));

// Enable CORS middleware
app.use((req, res, next) => {
  const allowedOrigins = ['http://localhost:5500', 'http://127.0.0.1:5500'];
  const origin = req.headers.origin;
  if (allowedOrigins.includes(origin)) {
    res.setHeader('Access-Control-Allow-Origin', origin);
  }
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

// Route to serve index.html for other routes
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', 'index.html'));
});

// Route for user login
app.post('/login', (req, res) => {
  const { email, password } = req.body;

  console.log('Login attempt with email:', email);

  db.get('SELECT * FROM Kunde WHERE email = ? AND password = ?', [email, password], (err, row) => {
    if (err) {
      console.error('Error fetching user:', err);
      res.status(500).json({ success: false, message: 'Internal Server Error' });
    } else if (row) {
      req.session.kundeID = row.kundeID; // Save kundeID in session
      res.json({ success: true });
    } else {
      res.status(401).json({ success: false, message: 'Invalid credentials' });
    }
  });
});


// Route to get profile data by kundeID
app.get('/profile/:kundeID', (req, res) => {
  const kundeID = req.params.kundeID;
  db.get('SELECT * FROM Kunde WHERE kundeID = ?', [kundeID], (err, row) => {
    if (err) {
      console.error('Error fetching profile:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else if (!row) {
      res.status(404).json({ error: 'Profile not found' });
    } else {
      res.json(row);
    }
  });
});

// Route to fetch QR code for the current user
app.get('/kunde_qrcode', (req, res) => {
  const kundeID = req.session.kundeID; // Retrieve kundeID from session
  // Query database to fetch QR code based on kundeID
  db.get('SELECT qrCode FROM Kunde WHERE kundeID = ?', [kundeID], (err, row) => {
      if (err) {
          console.error('Error fetching QR code:', err);
          res.status(500).json({ success: false, message: 'Internal Server Error' });
      } else if (row && row.qrCode) {
          res.json({ success: true, qrCode: row.qrCode });
      } else {
          res.status(404).json({ success: false, message: 'QR code not found for this user' });
      }
  });
});

// Route to get all products
app.get('/products', (req, res) => {
  db.all('SELECT * FROM Produkt', (err, rows) => {
    if (err) {
      console.error('Error fetching products:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else {
      const products = rows.map(row => {
        // Convert the bild Buffer to a base64 string
        row.bild = row.bild.toString('base64');
        return row;
      });
      res.json(products);
    }
  });
});

// Route to get a single product by ID
app.get('/product_detail/:produktID', (req, res) => {
  const produktID = req.params.produktID;
  db.get('SELECT * FROM Produkt WHERE produktID = ?', [produktID], (err, row) => {
    if (err) {
      console.error('Error fetching product:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else if (!row) {
      res.status(404).json({ error: 'Product not found' });
    } else {
      res.json(row);
    }
  });
});

// Route to get all vouchers
app.get('/vouchers', (req, res) => {
  db.all('SELECT * FROM Gutschein', (err, rows) => {
    if (err) {
      console.error('Error fetching vouchers:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else {
      const vouchers = rows.map(row => {
        // Convert the bild Buffer to a base64 string
        row.bild = row.bild.toString('base64');
        return row;
      });
      res.json(vouchers);
    }
  });
});

// Route to get a single voucher by ID
app.get('/voucher_detail/:gutscheinID', (req, res) => {
  const gutscheinID = req.params.gutscheinID;
  db.get('SELECT * FROM Gutschein WHERE gutscheinID = ?', [gutscheinID], (err, row) => {
    if (err) {
      console.error('Error fetching voucher:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else if (!row) {
      res.status(404).json({ error: 'Voucher not found' });
    } else {
      // Convert the bild Buffer to a base64 string
      row.bild = row.bild.toString('base64');
      res.json(row);
    }
  });
});

// Route to redeem a voucher and save to Kunde_Gutschein
app.post('/redeem/:gutscheinID', (req, res) => {
  const { gutscheinID } = req.params;
  const kundeID = req.session.kundeID; // Assuming kundeID is stored in session

  // Save redemption to Kunde_Gutschein table
  const currentDate = new Date().toISOString().slice(0, 10); // Get current date in 'YYYY-MM-DD' format
  db.run('INSERT INTO Kunde_Gutschein (kundeID, gutscheinID, eingeloestDatum) VALUES (?, ?, ?)', [kundeID, gutscheinID, currentDate], (err) => {
    if (err) {
      console.error('Error redeeming voucher:', err);
      res.status(500).json({ error: 'Internal Server Error' });
    } else {
      res.json({ success: true, message: 'Voucher redeemed successfully' });
    }
  });
});

// Route to fetch QR code for a voucher
app.get('/qrcode/:gutscheinID', (req, res) => {
  const { gutscheinID } = req.params;
  // Query database to fetch QR code based on gutscheinID
  db.get('SELECT qrCode FROM Gutschein WHERE gutscheinID = ?', [gutscheinID], (err, row) => {
    if (err) {
        console.error('Error fetching QR code:', err);
        res.status(500).json({ success: false, message: 'Internal Server Error' });
    } else if (row && row.qrCode) {
        res.json({ success: true, qrCode: row.qrCode, gutscheinID: gutscheinID});
    } else {
        res.status(404).json({ success: false, message: 'QR code not found for this voucher' });
    }
  });
});






app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
