## Aufgabenstellung
Erstellen Sie sich ein Repository in Github oder GitLab.
    Pushen Sie ein eigenes Projekt von Ihnen hoch (z.B. das CCD-Projekt) oder erstellen Sie ein neues Projekt!
    Wenden Sie alle in den Unterlagen genannten relevanten Methoden beweisbar an: (das Github Repo ist Beweis)
  ## pull
    Hier habe ich als Vorbereitung in Gitlb selbst qrcode.html und style.css angepasst, damit ich etwas zu pullen habe: ![Pull Vorbereitung](ESA_Screenshots/pull_vorbereitung.png)
    Hier habe ich diese Veränderung in mein lokales Repository gepullt: ![git pull](ESA_Screenshots/git_pull.png)
  ## add, commit, pus
    Hier habe ich products.html geändert, über add zur Staging Umgebung hinzugefügt und committed und gepusht, wobei ich Änderungen in der Readme vorerst ignoriert habe: ![git add commit push](ESA_Screenshots/git_add_commit_push.png)
  ## diff
    Hier habe ich mir die Unterschiede nach Bearbeitung der Readme anzeigen lassen: ![git diff](ESA_Screenshots/git_diff.png)
  ## status
    Hier habe ich mir den aktuellen Status anzeigen lassen: ![git status](ESA_Screenshots/git_status.png)
  ## rm
 Hier habe ich nach Einfügen eines belanglosen Screenshots diesen gelöscht: ![git rm](ESA_Screenshots/git_rm.png)
  ## 
  Hier habe ich die Bilder dieser Readme durch mv in ein Verzeichnis verschoben und dies gemeinsam mit der veränderten Readme durch angepasste Bildpfade comittet und gepusht: ![git mv](ESA_Screenshots/git_mv.png)
  ## Experimentieren Sie mit Zeitreisen!
  Checkout auf alten Commit nach Anzeige der Historie über git log:  ![git log checkout](ESA_Screenshots/git_log_checkout.png)
  Zurück wechseln zu aktuellen Stand: ![git checkout main](ESA_Screenshots/git_checkout_main.png)
  Anzeige der Unterschiede zu früheren Commit: ![git diff hash main](ESA_Screenshots/git_diff_hash.png)
  ## Erstellen sie zwei unterschiedliche aber ähnliche Branches, wechseln sie hin und her und mergen sie diese Branches dann wieder!
  ## Erstellen Sie in GitHub einen Pull-Request bezugnehmend auf https://github.com/edlich/education! Bitte referenzieren Sie auf den Pull-Request mit Link oder der Pull-Request Nummer! Kryptische GitHub Namen kann ich kaum zuordnen. Die Aufgabenteile vor dem Pull-Request bitte nicht in den Pull-Request einbauen, sondern extra abgeben!

//Machen sie ihr Repo public! (Bitte keine Einladungen)

Ideal ist die Abgabe eines Links mit allen Aufgaben ggf. Screenshots in der Readme.md!