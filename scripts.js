document.addEventListener('DOMContentLoaded', function() {
    const carouselInner = document.getElementById('carousel-inner');
    const nextButton = document.querySelector('.next');
    const prevButton = document.querySelector('.prev');

    const productsData = [
        {
            "title": "EcoFresh Bambus-Zahnbürste mit austauschbaren Bürstenköpfen",
            "image": "assets/EcofreshZahnbuerste.png"
        },
        {
            "title": "GrünGenuss Veganes Erbsenhack",
            "image": "/assets/GruenGenussErbsenhack.png"
        }
    ];

    const vouchersData = [
        {
            "title": "5€ auf deinen Einkauf",
            "image": "assets/5euroGutschein.png"
        },
        {
            "title": "10€ auf deinen Einkauf",
            "image": "/assets/10euroGutschein.png"
        }
    ];

    function showSlides(data) {
        carouselInner.innerHTML = '';
        data.forEach((item, index) => {
            const slideElement = document.createElement('div');
            slideElement.className = 'carousel-item' + (index === 0 ? ' active' : '');
            
            const img = new Image();
            img.onload = function() {
                slideElement.innerHTML = `<img src="${item.image}" alt="${item.title}"><h3>${item.title}</h3>`;
                carouselInner.appendChild(slideElement);
            };
            img.onerror = function() {
                console.error("Failed to load image at " + item.image);
                slideElement.innerHTML = `<img src="assets/placeholder.png" alt="Bild nicht verfügbar"><h3>${item.title}</h3>`; // Fallback-Bild
                carouselInner.appendChild(slideElement);
            };
            img.src = item.image; // Stellen Sie sicher, dass dieser Pfad korrekt ist
        });
    }
    
    

    function nextSlide() {
        const active = document.querySelector('.carousel-item.active');
        const next = active.nextElementSibling || carouselInner.firstElementChild;
        active.classList.remove('active');
        next.classList.add('active');
    }

    function prevSlide() {
        const active = document.querySelector('.carousel-item.active');
        const prev = active.previousElementSibling || carouselInner.lastElementChild;
        active.classList.remove('active');
        prev.classList.add('active');
    }

    nextButton.addEventListener('click', nextSlide);
    prevButton.addEventListener('click', prevSlide);

    // Dynamisch bestimmen, welche Seite geladen wird
    if (window.location.href.includes('products.html')) {
        showSlides(productsData);
    } else if (window.location.href.includes('vouchers.html')) {
        showSlides(vouchersData);
    }
});
