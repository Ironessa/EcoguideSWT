const sqlite3 = require('sqlite3').verbose();
const qrcode = require('qrcode');
const { promisify } = require('util');
const fs = require('fs');
const path = require('path');

// Function to generate QR code and encode it as base64
async function generateQRCode(data) {
    try {
        const options = {
            errorCorrectionLevel: 'H', // High error correction level
            margin: 0, // Set border size to 4 modules
            scale: 10, // Increase size of QR code
        };
        const qrCodeDataUrl = await promisify(qrcode.toDataURL)(data, options);
        return qrCodeDataUrl.replace(/^data:image\/png;base64,/, '');
    } catch (error) {
        console.error('Error generating QR code:', error);
        throw error;
    }
}

// Fake data
const kundenData = [
    {
        kundeID: 'k101',
        name: 'Max Mustermann',
        email: 'max.mustermann@example.com',
        password: 'password101',
        gesammeltePunkte: 100,
        qrCode: '',
    },
    {
        kundeID: 'k102',
        name: 'Maria Musterfrau',
        email: 'maria.musterfrau@example.com',
        password: 'password102',
        gesammeltePunkte: 150,
        qrCode: '',
    },
];

const produktData = [
    {
        produktID: 1,
        name: 'EcoFresh Bambus-Zahnbürste mit austauschbaren Bürstenköpfen',
        beschreibung: `Entdecke die EcoFresh Bambus-Zahnbürste – die perfekte Kombination aus Stil, Funktionalität und Umweltbewusstsein. Unsere innovative Bambus-Zahnbürste wurde entwickelt, um dir eine effektive und nachhaltige Zahnpflege zu bieten. Das umweltfreundliche Griffdesign aus hochwertigem Bambus liegt angenehm in der Hand und verleiht deiner Zahnpflege-Routine einen Hauch von Natürlichkeit. 
        Was die EcoFresh Bambus-Zahnbürste einzigartig macht, ist ihr innovatives Konzept austauschbarer Bürstenköpfe. Anstatt die gesamte Zahnbürste wegzuwerfen, wenn die Borsten abgenutzt sind, kannst du einfach den Bürstenkopf austauschen und die Zahnbürste weiterverwenden. Dies reduziert nicht nur den Abfall, sondern spart auch Kosten und Ressourcen.
        Mit ihrer effektiven Reinigungsleistung und ihrem umweltfreundlichen Design ist die EcoFresh Bambus-Zahnbürste die perfekte Wahl für umweltbewusste Verbraucher, die nachhaltige Alternativen suchen, ohne auf Qualität zu verzichten. Mach noch heute den Schritt zu einer grüneren Zahnpflege-Routine mit der EcoFresh Bambus-Zahnbürste!`,
        preis: 1.5,
        punkte: 10,
        bild: fs.readFileSync(path.join(__dirname, '../assets/EcofreshZahnbuerste.png')),
    },
    {
        produktID: 2,
        name: 'GrünGenuss Veganes Erbsenhack - Lokal und Nachhaltig',
        beschreibung: `Willkommen bei GrünGenuss, wo Qualität auf Nachhaltigkeit trifft! Unser veganes Erbsenhack ist nicht nur eine köstliche Alternative zu herkömmlichem Hackfleisch, sondern auch eine umweltfreundliche Wahl für bewusste Genießer. Hergestellt aus hochwertigen heimischen Erbsen und sorgfältig ausgewählten Zutaten, bietet unser Erbsenhack einen unvergleichlichen Geschmack und ernährungsphysiologische Vorteile, ohne Kompromisse bei der Umweltfreundlichkeit einzugehen.
        Unser Erbsenhack wird lokal produziert und unterstützt nachhaltige Landwirtschaftspraktiken, die den ökologischen Fußabdruck minimieren und die Biodiversität fördern. Mit seinem hohen Gehalt an pflanzlichen Proteinen und Ballaststoffen ist unser Erbsenhack nicht nur gut für dich, sondern auch für unseren Planeten.
        Egal, ob du ein erfahrener Veganer bist oder einfach nur nach neuen, nachhaltigen Ernährungsoptionen suchst, unser GrünGenuss Veganes Erbsenhack wird dich begeistern. Genieße den Geschmack der Zukunft und trage gleichzeitig dazu bei, eine nachhaltigere Welt zu schaffen – mit GrünGenuss ist es einfach, grün zu genießen!`,
        preis: 5.0,
        punkte: 20,
        bild: fs.readFileSync(path.join(__dirname, '../assets/GruenGenussErbsenhack.png')),
    },
];

const haendlerData = [
    { haendlerID: 1, name: 'REWE', anschrift: 'Musterstraße 1, Beispielstadt', produktSortiment: '1,2' },
    { haendlerID: 2, name: 'Lidl', anschrift: 'Musterweg 2, Umweltstadt', produktSortiment: '2,1' },
];

const einkaufData = [
    { einkaufID: 1, kundeID: 'k101', gesammeltePunkte: 10, preisGesamt: 15.0, einkaufsdatum: new Date().toISOString(), gekaufteProdukte: '1,2', gutscheinID: 'X0D54-DG3T5-1F2G6', haendlerID: 1 },
    { einkaufID: 2, kundeID: 'k102', gesammeltePunkte: 20, preisGesamt: 30.0, einkaufsdatum: new Date().toISOString(), gekaufteProdukte: '2,1', gutscheinID: 'SE4E3-34F54-09E6T', haendlerID: 2 },
];

const gutscheinData = [
    {
        gutscheinID: 'X0D54-DG3T5-1F2G6',
        titel: '5 € Gutschein auf deinen Einkauf',
        beschreibung: `Bei EcoGuide belohnen wir jeden Schritt, den du in Richtung eines nachhaltigeren Lebens gehst.. Löse diesen 5 Euro Gutschein ein und erhalte zusätzliche Punkte, die du gegen tolle Prämien oder Spenden an Umweltprojekte deiner Wahl eintauschen kannst. Jeder Einkauf zählt, jeder Punkt macht einen Unterschied – für dich und unseren Planeten!`,
        haendler: null,
        wert: 5,
        gueltigkeit: 1,
        produkte: null,
        qrCode: '',
        bild: fs.readFileSync(path.join(__dirname, '../assets/5euroGutschein.png')),
    },
    {
        gutscheinID: 'SE4E3-34F54-09E6T',
        titel: '10 € Gutschein auf deinen Einkauf',
        beschreibung: `Als Dankeschön für deine Entscheidung, nachhaltig zu leben, schenkt dir EcoGuide einen Gutschein über 10 Euro. Tausche deine Punkte gegen spezielle Angebote, Gutscheine oder unterstütze Umweltschutzprojekte, die dir am Herzen liegen. Von öko-zertifizierter Kosmetik bis hin zu nachhaltiger Mode – lass dich inspirieren und belohne dich selbst mit Qualität, die auch der Erde guttut.`,
        haendler: 0,
        wert: 10,
        gueltigkeit: 1,
        produkte: null,
        qrCode: '',
        bild: fs.readFileSync(path.join(__dirname, '../assets/10euroGutschein.png')),
    },
];

const kundeGutscheinData = [
    { id: 1, kundeID: 'd101', gutscheinID: 'X0D54-DG3T5-1F2G6', eingeloestDatum: new Date().toISOString(), verwendetDatum: '' },
    { id: 2, kundeID: 'd102', gutscheinID: 'SE4E3-34F54-09E6T', eingeloestDatum: new Date().toISOString(), verwendetDatum: '' },
];

// Function to create tables
async function createTables(db) {
    return new Promise((resolve, reject) => {
        db.serialize(() => {
            db.run(
                `CREATE TABLE IF NOT EXISTS Kunde (
                    kundeID TEXT PRIMARY KEY,
                    name TEXT,
                    email TEXT,
                    password TEXT,
                    gesammeltePunkte INTEGER,
                    qrCode TEXT
                )`
            );

            db.run(
                `CREATE TABLE IF NOT EXISTS Produkt (
                    produktID INTEGER PRIMARY KEY,
                    name TEXT,
                    beschreibung TEXT,
                    preis REAL,
                    punkte INTEGER,
                    bild BLOB
                )`
            );

            db.run(
                `CREATE TABLE IF NOT EXISTS Haendler (
                    haendlerID INTEGER PRIMARY KEY,
                    name TEXT,
                    anschrift TEXT,
                    produktSortiment TEXT
                )`
            );

            db.run(
                `CREATE TABLE IF NOT EXISTS Einkauf (
                    einkaufID INTEGER PRIMARY KEY,
                    kundeID TEXT,
                    gesammeltePunkte INTEGER,
                    preisGesamt REAL,
                    einkaufsdatum TEXT,
                    gekaufteProdukte TEXT,
                    gutscheinID TEXT,
                    haendlerID INTEGER,
                    FOREIGN KEY (kundeID) REFERENCES Kunde(kundeID),
                    FOREIGN KEY (gutscheinID) REFERENCES Gutschein(gutscheinID),
                    FOREIGN KEY (haendlerID) REFERENCES Haendler(haendlerID)
                )`
            );

            db.run(
                `CREATE TABLE IF NOT EXISTS Gutschein (
                    gutscheinID TEXT PRIMARY KEY,
                    titel TEXT,
                    beschreibung TEXT,
                    haendler INTEGER,
                    wert REAL,
                    gueltigkeit INTEGER,
                    produkte TEXT,
                    qrCode TEXT,
                    bild BLOB
                )`
            );

            db.run(
                `CREATE TABLE IF NOT EXISTS Kunde_Gutschein (
                    id INTEGER PRIMARY KEY,
                    kundeID TEXT,
                    gutscheinID TEXT,
                    eingeloestDatum TEXT,
                    verwendetDatum TEXT,
                    FOREIGN KEY (kundeID) REFERENCES Kunde(kundeID),
                    FOREIGN KEY (gutscheinID) REFERENCES Gutschein(gutscheinID)
                )`,
                (err) => {
                    if (err) reject(err);
                    else resolve();
                }
            );
        });
    });
}

// Function to insert data
async function insertData(db) {
    try {
        // Insert data into Kunde table
        const kundenStmt = db.prepare('INSERT INTO Kunde (kundeID, name, email, password, gesammeltePunkte, qrCode) VALUES (?, ?, ?, ?, ?, ?)');
        for (const data of kundenData) {
            data.qrCode = await generateQRCode(data.kundeID); // Assuming kundeID is the data to generate QR code
            kundenStmt.run(data.kundeID, data.name, data.email, data.password, data.gesammeltePunkte, data.qrCode);
        }
        kundenStmt.finalize();

        // Insert data into Produkt table
        const produktStmt = db.prepare('INSERT INTO Produkt (produktID, name, beschreibung, preis, punkte, bild) VALUES (?, ?, ?, ?, ?, ?)');
        for (const data of produktData) {
            produktStmt.run(data.produktID, data.name, data.beschreibung, data.preis, data.punkte, data.bild);
        }
        produktStmt.finalize();

        // Insert data into Haendler table
        const haendlerStmt = db.prepare('INSERT INTO Haendler (haendlerID, name, anschrift, produktSortiment) VALUES (?, ?, ?, ?)');
        for (const data of haendlerData) {
            haendlerStmt.run(data.haendlerID, data.name, data.anschrift, data.produktSortiment);
        }
        haendlerStmt.finalize();

        // Insert data into Einkauf table
        const einkaufStmt = db.prepare('INSERT INTO Einkauf (einkaufID, kundeID, gesammeltePunkte, preisGesamt, einkaufsdatum, gekaufteProdukte, gutscheinID, haendlerID) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
        for (const data of einkaufData) {
            einkaufStmt.run(data.einkaufID, data.kundeID, data.gesammeltePunkte, data.preisGesamt, data.einkaufsdatum, data.gekaufteProdukte, data.gutscheinID, data.haendlerID);
        }
        einkaufStmt.finalize();

        // Insert data into Gutschein table
        const gutscheinStmt = db.prepare('INSERT INTO Gutschein (gutscheinID, titel, beschreibung, haendler, wert, gueltigkeit, produkte, qrCode, bild) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
        for (const data of gutscheinData) {
            data.qrCode = await generateQRCode(data.gutscheinID); // Assuming gutscheinID is the data to generate QR code
            gutscheinStmt.run(data.gutscheinID, data.titel, data.beschreibung, data.haendler, data.wert, data.gueltigkeit, data.produkte, data.qrCode, data.bild);
        }
        gutscheinStmt.finalize();

        // Insert data into Kunde_Gutschein table
        const kundeGutscheinStmt = db.prepare('INSERT INTO Kunde_Gutschein (id, kundeID, gutscheinID, eingeloestDatum, verwendetDatum) VALUES (?, ?, ?, ?, ?)');
        for (const data of kundeGutscheinData) {
            kundeGutscheinStmt.run(data.id, data.kundeID, data.gutscheinID, data.eingeloestDatum, data.verwendDatum);
        }
        kundeGutscheinStmt.finalize();

        console.log('Data inserted successfully.');
    } catch (err) {
        console.error('Error inserting data:', err.message);
    }
}

async function main() {
    const dbPath = path.join(__dirname, 'ecoguide.db');

    // Create the new database
    const db = new sqlite3.Database(dbPath, (err) => {
        if (err) {
            console.error('Could not create database', err);
        } else {
            console.log('Database created');
        }
    });

    try {
        await createTables(db);
        console.log('Tables created successfully.');
        await insertData(db);
    } catch (err) {
        console.error('Error creating tables or inserting data:', err.message);
    } finally {
        db.close((err) => {
            if (err) {
                console.error('Error closing the database:', err.message);
            } else {
                console.log('Database connection closed.');
            }
        });
    }
}

main();
