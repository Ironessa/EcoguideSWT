const sqlite3 = require('sqlite3').verbose();
const path = require('path');

const dbPath = path.join(__dirname, 'ecoguide.db');

let db = new sqlite3.Database(dbPath, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the ecoguide database.');
});

const tables = ['Kunde', 'Produkt', 'Haendler', 'Einkauf', 'Gutschein', 'Kunde_Gutschein'];

db.serialize(() => {
  tables.forEach(table => {
    db.get(`SELECT COUNT(*) as count FROM ${table}`, (err, row) => {
      if (err) {
        console.error(`Error when retrieving data from ${table}:`, err.message);
      } else {
        console.log(`Table ${table} has ${row.count} rows.`);
      }
    });
  });

  db.each(`SELECT * FROM Kunde`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });

  db.each(`SELECT * FROM Produkt`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });

  db.each(`SELECT * FROM Haendler`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });

  db.each(`SELECT * FROM Einkauf`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });

  db.each(`SELECT * FROM Gutschein`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });

  db.each(`SELECT * FROM Kunde_Gutschein`, (err, row) => {
    if (err) {
      console.error(err.message);
    }
    console.log(row);
  });
});

db.close((err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Close the database connection.');
});